## Installieren von Git für Windows
https://git-scm.com/download/win

## Installieren unter Linux 
http://git-scm.com/download/linux

## Installiern unter Mac 
http://git-scm.com/download/mac
`brew install git`

## Repository Auschecken 
`git clone --bare git@gitlab.com:skillsforteams/gitlabcourse.git`


## Mehr Tutorials auch auf meinem Twitch Kanal
twitch.tv/skillsforteams

## Kontakt
https://skills-for-teams.com/
