# Git Befehle 

`git log `

Show last changes
`git show <commitId>`
Zeigt aktuellen commit


`git diff HEAD HEAD^ agenda.md`

Vergleiche die Datei Agenda.md mit der vorherigen Version


`git reset <filename>`

Ein file von der stage zurücknehmen

`git reset --hard`

Working copy auf den HEAD zurücksetzen.

`git commit -m "Commit Text Message"`
Dateien die aktuell auf der Stage sind commiten.

`git push`
dateien auf das default remote schieben

`git pull`
Änderungen vom remote repositiory ziehen
(git fetch & git merge )

`git merge <branchname>`
<branchname> in den aktuellen branch mergen
Branch auf basis zurücksetzen.
Git Rebase damit kann man diskussionen entfachen ...

`git rebase`
Git Rebase damit kann man diskussionen entfachen ... 

`git cherry-pick <commitid>`
Anwenden eines commits auf den aktuellen branch

# TreeIsh

`HEAD^` `HEAD~1` vor 1 commit
`HEAD^^^^` `HEAD~4` vor 4 commits

Treeish ist die syntax um einen commit zu identifizieren
und funktioniert mit Zeigern oder commitId



